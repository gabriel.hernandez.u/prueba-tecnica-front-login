import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import {Router,RouterOutlet } from '@angular/router';
import { WelcomeComponent } from "./welcome.component";


@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  
})
export class AppComponent implements OnInit {
  user = {
    email: null,
    password: null
  };

  constructor(private toastr: ToastrService,private router:Router) {}
  ngOnInit(): void {
    throw new Error("Method not implemented.");
  }

  onSubmit(user: object) {
    this.toastr.show("User logged in successfully");
  
    console.log(user);
    this.router.navigate(['/welcome']);
    
  }
}