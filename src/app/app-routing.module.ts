import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WelcomeComponent } from './welcome.component';

import { AppComponent } from './app.component';


const routes: Routes = [ 
  { path: '', component: AppComponent },
  { path: 'welcome', component: WelcomeComponent },
 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
